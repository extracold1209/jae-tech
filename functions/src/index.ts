import {https} from 'firebase-functions';
import express from 'express';
import mainApp from './main';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const rootApp = express();
rootApp.use('/api', mainApp);

// @ts-ignore
// noinspection JSUnusedGlobalSymbols
export const main = https.onRequest(rootApp);
