import React from 'react';
import { addDecorator, addParameters } from '@storybook/react';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import CssBaseline from '@material-ui/core/CssBaseline';

addParameters({
    viewport: {
        viewports: INITIAL_VIEWPORTS,
    },
});

addDecorator(storyFn =>
    <>
        <CssBaseline/>
        {storyFn()}
    </>
);
