import React from 'react';
import Container from '@material-ui/core/Container';
import Footer from '../../molecules/Footer';
import Header from '../../molecules/Header';
import {makeStyles} from '@material-ui/core/styles';
import DepositInterestTemplate from './DepositInterestTemplate';

const useStyles = makeStyles((theme) => ({
    mainContainer: {
        marginBottom: theme.spacing(4),
    },
}));

const MainContainer: React.FC = () => {
    const classes = useStyles();

    return (
        <>
            <Header/>
            <Container
                maxWidth='xl'
                className={classes.mainContainer}
            >
                <DepositInterestTemplate/>
            </Container>
            <Footer/>
        </>
    );
};

export default MainContainer;
