import {storiesOf} from '@storybook/react';
import React from 'react';
import MainContainer from './index';
import DepositInterestTemplate from './DepositInterestTemplate';

storiesOf('Pages.Index', module)
    .add('deposit', () => <DepositInterestTemplate/>)
    .add('default', () => <MainContainer/>);
