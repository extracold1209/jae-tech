import React, {useMemo, useState} from 'react';
import {ceil} from 'lodash';
import {Box} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Divider from '@material-ui/core/Divider';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import CurrencyTextField from '@unicef/material-ui-currency-textfield';
import Button from '@material-ui/core/Button';
import {Textfit} from 'react-textfit';
import Paper from '@material-ui/core/Paper';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    contentContainer: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    depositAdderContainer: {
        display: 'inherit',
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    depositAddButton: {
        width: '33.33%',
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    divider: {
        width: '100%'
    }
}));

const DepositInterestTemplate: React.FC = () => {
    const classes = useStyles();
    const [deposit, setDeposit] = useState(1000000);
    const [interest, setInterest] = useState(1.5);
    const [years, setYears] = useState('1');
    const [isIncludeTax, setTaxInclude] = useState('true');
    const calculatedInterest = useMemo(() =>
        ceil(deposit * (interest / 100 * (isIncludeTax === 'true' ? 0.846 : 1)) * Number.parseInt(years), 10)
    , [interest, deposit, years, isIncludeTax]);

    return (
        <Paper elevation={3} className={classes.contentContainer}>
            <Box mb={4}>
                <Typography component='h1' variant='h4' align='center'>
                    예금 이자 계산기
                </Typography>
            </Box>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <CurrencyTextField
                        required
                        currencySymbol='₩'
                        minimumValue='0'
                        decimalPlaces='0'
                        id='deposit'
                        variant='outlined'
                        label='예금액'
                        value={deposit}
                        onChange={(event: any, value: number) => {
                            setDeposit(value);
                        }}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} sm={6} className={classes.depositAdderContainer}>
                    <Button variant='contained' className={classes.depositAddButton}
                        onClick={() => setDeposit(deposit + 5000000)}>
                        <Textfit mode='single'>+500만원</Textfit>
                    </Button>
                    <Button variant='contained' className={classes.depositAddButton}
                        onClick={() => setDeposit(deposit + 1000000)}>
                        <Textfit mode='single'>+100만원</Textfit>
                    </Button>
                    <Button variant='contained' className={classes.depositAddButton}
                        onClick={() => setDeposit(deposit + 100000)}>
                        <Textfit mode='single'>+10만원</Textfit>
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    <CurrencyTextField
                        required
                        currencySymbol='%'
                        minimumValue='0'
                        id='percent'
                        variant='outlined'
                        label='이자율'
                        value={interest}
                        onChange={(event: any, value: number) => {
                            setInterest(value);
                        }}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12}>
                    <FormControl component='fieldset'>
                        <FormLabel component='legend'>예치년수</FormLabel>
                        <RadioGroup row defaultValue={years} onChange={((event, value) => {
                            setYears(value);
                        })}>
                            <FormControlLabel value='1' control={<Radio color='primary'/>} label='1년'/>
                            <FormControlLabel value='2' control={<Radio color='primary'/>} label='2년'/>
                            <FormControlLabel value='3' control={<Radio color='primary'/>} label='3년'/>
                        </RadioGroup>
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                    <FormControl component='fieldset'>
                        <FormLabel component='legend'>세금포함여부 (15.4%)</FormLabel>
                        <RadioGroup row defaultValue={isIncludeTax} onChange={(event, value) => {setTaxInclude(value);}} >
                            <FormControlLabel value='true' control={<Radio color='primary'/>} label='포함'/>
                            <FormControlLabel value='false' control={<Radio color='primary'/>} label='미포함'/>
                        </RadioGroup>
                    </FormControl>
                </Grid>
                <Divider variant='fullWidth' className={classes.divider}/>
                <Grid item xs={12}>
                    <Typography variant='subtitle1'>이자 : {Number((calculatedInterest).toFixed(1)).toLocaleString()} 원</Typography>
                    <Typography variant='subtitle1'>만기지급액 : {Number((calculatedInterest + deposit).toFixed(1)).toLocaleString()} 원</Typography>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default DepositInterestTemplate;
