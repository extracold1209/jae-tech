# Components Structure

- atoms: 내부에서 더이상 조립되지 않는 최소단위. Component Framework 만으로의 조합은 가능하다. UI / EventEmit 용 이벤트만 발생시킨다.
- molecules: atoms 및 Component Framework 의 조합으로 이루어지며, 자체적인 state / actions 및 로직을 가질 수 있다. 공용으로 사용되어야 한다.
- pages: 요청에 의해 보여지는 최종단계의 컴포넌트. 페이지 디렉토리는 도메인단위로 구성한다.
  - pages 내 component 는 전부 해당 도메인 내에서만 사용되어야 한다. 다른 page 에서 불려서는 안된다.
