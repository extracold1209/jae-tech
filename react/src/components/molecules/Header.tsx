import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: theme.spacing(2)
    }
}));

const Header: React.FC = () => {
    const classes = useStyles();
    return (
        <AppBar position='sticky' color='default' className={classes.root}>
            <Toolbar>
                <Typography variant='h6' color='inherit' noWrap>
                    Company name
                </Typography>
            </Toolbar>
        </AppBar>
    );
};

export default Header;
