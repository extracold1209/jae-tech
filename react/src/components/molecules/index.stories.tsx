import React from 'react';
import {storiesOf} from '@storybook/react';
import Footer from './Footer';
import Header from './Header';

storiesOf('Molecules', module)
    .add('Header', () => <Header/>)
    .add('Footer', () => <Footer/>);
