import React from 'react';
import Container from '@material-ui/core/Container';
import DemoCopyright from '../atoms/DemoCopyright';

const Footer: React.FC = () => (
    <Container maxWidth='lg'>
        <DemoCopyright/>
    </Container>
);

export default Footer;
