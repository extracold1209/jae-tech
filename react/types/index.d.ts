declare module '@rebass/preset' {
    // noinspection ES6ConvertVarToLetConst
    var value: any;
    export = value;
}

declare module '*.png' {
    var value: any;
    export = value;
}

declare module 'react-textfit' {
    var Textfit: any;
    export {
        Textfit
    };
}

declare module '@unicef/material-ui-currency-textfield' {
    var value: any;
    export = value;
}
